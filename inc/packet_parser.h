#ifndef PACKET_PARSER_H
#define PACKET_PARSER_H

//
// Note: Hardcoded to work with specific packet
//
// Input - one packet containing delta theta, temp fields
// Output - DeltaX, DeltaY, DeltaZ, Temperature
//
void parse_3float_temp_packet(unsigned char raw_packet[], float output_values[], int* num_values);

#endif