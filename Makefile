TARGET=main

## linker
LINK = gcc

## Directories and extensions
BIN=bin
SRC=src
OBJ=obj
INC=inc
EXT=c

## Turn on optimization and warnings:
CFLAGS = -Wall -O -g
CXXFLAGS = $(CFLAGS)

LIBS = -lserialport

#===========================================

C_FILES := $(wildcard $(SRC)/*.$(EXT))
OBJ_FILES := $(addprefix $(OBJ)/,$(notdir $(C_FILES:.$(EXT)=.o)))

#===========================================

$(BIN)/$(TARGET): $(OBJ_FILES)
			mkdir -p $(BIN)
		    $(LINK) -o $@ $^ $(LIBS)

$(OBJ)/%.o: $(SRC)/%.$(EXT)
			mkdir -p $(OBJ)
		    $(LINK) -c -o $@ $< $(CXXFLAGS) -I$(INC)

#===========================================

clean:
		rm -f $(OBJ)/*.o *~ $(BIN)/$(TARGET)

