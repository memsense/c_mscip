# c_mscip

c_mscip is a C library for interfacing with Memsense IMU devices.

# Linux

The following is the process followed to build and run the library on Ubuntu 20.04. Some of the commands may require sudo privileges depending on your system's setup.

Install the needed dependencies.

`apt install make gcc libserialport-dev`

Build the project from the root directory.

`make`

Run the program on the connected device's port.

`./bin/main <baudrate>`

# Windows

Installing and running on windows requires using mingw-w64 through MSYS2. Download and installation instructions can be found on the [projects webpage](https://mingw-w64.org/doku.php/download). Once installed, the following instructions will need to be done on the MYSYS2 MinGW terminal.

Install the [libserialport](https://github.com/martinling/libserialport) library. Start by installing the system packages needed to compile the library.

`pacman -S autoconf automake-wrapper libtool make mingw-w64-x86_64-gcc`

The project can now be built and installed from the root directory.

`./autoconf.sh`

`./configure `

`make`

`make install`

The c_mscip project can now be built. Navigate to the project's root and build the the project.

`make`

The executable can now be ran. MinGW-w64 passes serial ports with the same name as the host windows system.

`./bin/main.exe <comport>`

or more specifically

`./bin/main.exe COM3`