#include "packet_parser.h"

float get_float(unsigned char* data);

#define X_IDX 6
#define Y_IDX 10
#define Z_IDX 14
#define T_IDX 20 

void parse_3float_temp_packet(unsigned char raw_packet[], float output_values[], int* num_values)
{
    output_values[0] = get_float(&raw_packet[X_IDX]);
    output_values[1] = get_float(&raw_packet[Y_IDX]);
    output_values[2] = get_float(&raw_packet[Z_IDX]);
    output_values[3] = get_float(&raw_packet[T_IDX]);
    *num_values = 4;
}

float get_float(unsigned char* data)
{    
    int tmp = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
    return *(float*)(&tmp);
}