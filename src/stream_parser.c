#include "stream_parser.h"

#define SYNCH_BYTE 0xA5
#define SYNCH_LENGTH 2

unsigned char tmp_buffer[PACKET_LEN];
int tmp_buffer_cnt = 0;
int synch_cnt = 0;
int packet_cnt = 0;

void parse_stream(unsigned char* input_buffer, int num_raw_bytes, 
    unsigned char packets[MAX_PACKETS][PACKET_LEN], int* num_packets)
{
    for (int i=0; i<num_raw_bytes; i++)
    {
        if (synch_cnt == SYNCH_LENGTH)   // All synched up
        {
            tmp_buffer[tmp_buffer_cnt] = input_buffer[i];
            tmp_buffer_cnt++;
            if (tmp_buffer_cnt == PACKET_LEN)
            {
                for (int j=0; j<PACKET_LEN; j++)
                    packets[packet_cnt][j] = tmp_buffer[j];
                packet_cnt++;
                synch_cnt = 0;
                tmp_buffer_cnt = 0;
            }
        }
        else if (input_buffer[i] == SYNCH_BYTE)
        {
            tmp_buffer[tmp_buffer_cnt] = input_buffer[i];
            tmp_buffer_cnt++;
            synch_cnt++;
        }
        else  // Something is wrong - reset
        {
            synch_cnt = 0;
            tmp_buffer_cnt = 0;
        }
    }

    *num_packets = packet_cnt;
    packet_cnt=0;
}
