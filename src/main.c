#include <stdio.h>
#include "libserialport.h"
#include "stream_parser.h"
#include "packet_parser.h"

#define RAW_BUFFER_SIZE 200

int main(int argc, char* argv[])
{
    enum sp_return result;
    char * com_port_name;
    struct sp_port* port;
    int baudrate = 460800;
    unsigned char packet_buffer[MAX_PACKETS][PACKET_LEN];
    int bytesRead;

    if (argc != 2)
    {
        printf("Usage: parse <comport>\n");
        return 1;
    }

    com_port_name = argv[1];
    printf("Connecting to %s ...\n", com_port_name);

    result = sp_get_port_by_name(com_port_name, &port);

    if (result != SP_OK)
    {
        printf("%s does not exist.\n", com_port_name);
        return 1;
    }

    result = sp_open(port, SP_MODE_READ);
    if (result != SP_OK)
    {
        printf("Error opening %s\n", com_port_name);
        return 1;
    }

    sp_set_baudrate(port, baudrate);

    unsigned char buffer[RAW_BUFFER_SIZE];

    while(1)
    {
        bytesRead = sp_blocking_read(port, buffer, RAW_BUFFER_SIZE, 1000);
        if (bytesRead > 0)
        {
            int num_packets, num_output_values;
            float values[4];
            parse_stream(buffer, bytesRead, packet_buffer, &num_packets);
        
            for (int i=0; i < num_packets; i++)
            {
                parse_3float_temp_packet(packet_buffer[i], values, &num_output_values);
                for (int k=0; k<num_output_values; k++)
                {
                    printf("%6.4f ", values[k]);
                }
                printf("\n");
            }
         }   
    }

    sp_close(port);
 
    return 0;
}
