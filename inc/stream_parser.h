#ifndef STREAM_PARSER_H
#define STREAM_PARSER_H

#define PACKET_LEN 26
#define MAX_PACKETS 10

//
// Note: Hardcoded to work with specific packet containing
//          containing only delta theta, temp fields
//
// Input - array of bytes
// Output - array of packets
//
void parse_stream(unsigned char* raw_buffer, int num_raw_bytes,
    unsigned char packets[MAX_PACKETS][PACKET_LEN], int* num_packets);
   
#endif